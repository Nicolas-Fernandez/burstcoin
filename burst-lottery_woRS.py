#!/usr/bin/env python
#
#####################################################################
# burst-address.py, a burst address generator in python             #
#                                                                   #
# execution requires installation of curve25519-donna;              #
# https://pypi.python.org/pypi/curve25519-donna                     #
# ex. of installation on linux:                                     #
# sudo pip install curve25519-donna                                 #
#                                                                   #
# test on linux using "Python 2.7.6" and "Python 3.6.0"             #
#                                                                   #
# written by damncourier in 2017, released public domain            #
#                                                                   #
# credit for code reuse would be nice, donations would be nicer ;)  #
# BURST-PVSK-HNTX-FLAA-HTRSX                                        #
#####################################################################
#
#####################################################################
# BURST adress ? Listen damncourier !
#
# 1) Passphrase (of whatever length) is hashed sha2 256 (same private key address space as bitcoin 256 bits)
#    seed --> Sha2_256 --> private_key
#
# 2) Hash output is used as private key in curve25519 to generate (32 byte) public key
#    private_key --> Curve_25519 --> public_key
#
# 3) Public key is hashed sha2 256
#    public_key --> Sha2_256 --> public_key_hash
#
# 4) First 8 bytes public key hash are bytes order reversed (little endian) and used as long id (https://en.wikipedia.org/wiki/Endianness)
#    public_key_hash first 8 bytes --> Little_Endian --> numeric_account_number
#
# 5) long id is displayed using nxt eed solomon for human readablity and error correction in entry (https://nxtwiki.org/wiki/RS_Address_Format)
#    numeric_account_number --> Reed_Solomon --> burst_adress (i.e. BURST-XXXX-XXXX-XXXX-XXXXX)
#
# seed = passphrase
# private_key = Sha2_256(seed)
# public_key = Curve_25519(private_key)
# public_key_hash = Sha2_256(public_key)
# numeric_account_number = Little_Endian(first_8_bytes(public_key_hash))
# burst_adress = Reed_Solomon(numeric_account_number)
#
# So, burst_adress = Reed_Solomon(Little_Endian(Sha2_256(Curve_25519(Sha2_256(seed)))))
#
#####################################################################
# burst-lottery.py, a burst LOTTERY in python                       #
#                                                                   #
# execution requires burst-address.py, thank damncourier !          #
# https://github.com/damncourier/burst-address.py                   #
#                                                                   #
# test on linux using "Python 2.7" and not "Python 3.5"             #
#                                                                   #
# written by Haradwaith in october 2018, released public domain     #
#                                                                   #
# credit for code reuse would be nice, donations would be nicer ;)  #
# BURST-YC6M-KUQS-2YMK-BCEMJ                                        #
#####################################################################
#
#####################################################################
# Based on original daWallet security challenge Apr 14, 2017.
# https://burstforum.net/topic/4766/the-canary-burst-early-warning-system
#
#  1 Word  BURST-GMVF-Z5L4-LGWZ-8BW6W #7931835070802251629  59dd8b0472c2f634f7bfbf6082e82a0aad59840340beea37a0d40a00a3502241 (entropy of  10 bits) 
#  2 Words BURST-ADFP-EN99-24FD-44QA7 #2495770882806656437  873b1b266ed4bb08f37dc2741ed4a8c021f45de1bde57397dcc3f9171cb68f69 (entropy of  21 bits) 
#  3 Words BURST-UP6D-R28A-67XL-DYBJL #13349019730708354187 1cdfe1e44baffc8416dc88ff9e182be5316aef49b0d888f03059d41521c1dd68 (entropy of  32 bits) 
#  4 Words BURST-24M6-5CWP-CPPZ-D9PVS #13577973659788642916 79846519098b7eed2516de591251bcc5a2b4a78be61e58ec3b011309e6459e48 (entropy of  42 bits) <<-- target !
#  5 Words BURST-RT5Y-YLDA-AZ5R-6T6MN #5353823332813497470  600c3133f9d40786a7df83be279db7f2a9478f94edc4a4b1d6b8f78438a45b30 (entropy of  53 bits)
#  6 Words BURST-AYQF-7YUJ-A88H-D32VQ #13505170686989335245 0f8759d21cffaa4631bd41738d6da44700e35f469b7fd38d99f3d3e230e0e700 (entropy of  64 bits)
#  7 Words BURST-ND84-WUE8-L9EZ-C27NB #11876169129636015298 155a83cb7fd3584fb9d09c7e6a468ba78c94b2ffffaf6f793a9c769c714d272a (entropy of  74 bits)
#  8 Words BURST-K8GV-VEAA-LRLS-F5C58 #15207886124854319579 c93e749961389ef25c7b204212762ead069e50c5be8e940c88f2a5b040b18b78 (entropy of  85 bits)
#  9 Words BURST-YRZY-WDWF-XQ35-AURDX #10281430852078362622 05225bb64286ffc70701f1bbf8fc5ce8d282ab50c8d7b5e96116f09276116325 (entropy of  96 bits)
# 10 Words BURST-RGGF-SJ88-272C-G3RXG #16678766502151436749 8ffe3298e186ce7d329208ca522081785fcac59ff1852cf6aee3243284581547 (entropy of 106 bits)
# 11 Words BURST-P7SA-89F9-62F4-53MEG #3977348287668328200  33b7f0b03ea5c0d08a5f9b50158b15b0cddd090341ccdf75ea2849f335c6cf55 (entropy of 117 bits)
# 12 Words BURST-6L9L-LULB-XVAZ-463RB #2656038109666494706  72d693d0e3eb17ab484cbb68e40edb9cb19c0e3e04a684b6325a479908451948 (entropy of 128 bits)
#
#####################################################################
####### burst-adress.py #######
#
####### import libraries
# access to command line arguments
import sys;
#
# sha2_256 digest function
from hashlib import sha256;
#
# curve25519-donna available from pip
# Private to set private key
from curve25519 import Private;
#
#####################################################################
####### burst-lottery.py #######
#
####### import libraries
import random
#
####### define targeted account from daWallet challange
#TARGET = 7931835070802251629  # 1 Words, 0 burstcoin - Time till cracked:  6 seconds with passphrase: "princess"           (record:)
#TARGET = 2495770882806656437  # 2 Words, 0 burstcoin - Time till cracked: 53 seconds with passphrase: "precious fault"     (record: 4 sec by blago)
#TARGET = 13349019730708354187 # 3 Words, 0 burstcoin - Time till cracked: 27 days    with passphrase: "ignore neither guy" (record: 5 hrs 41 min 8 sec by blago)
TARGET = 13577973659788642916 # 4 Words, 1'000 burstcoins -  Current target !
#TARGET = 5353823332813497470  # 5 Words, 1'000 burstcoins
#TARGET = 13505170686989335245 # 6 Words, 1'000 burstcoins
#TARGET = 11876169129636015298 # 7 Words, 1'000 burstcoins
#TARGET = 15207886124854319579 # 8 Words, 1'000 burstcoins
#TARGET = 10281430852078362622 # 9 Words, 1'000 burstcoins
#TARGET = 16678766502151436749 #10 Words, 1'000 burstcoins
#TARGET = 3977348287668328200  #11 Words, 1'000 burstcoins
#TARGET = 2656038109666494706  #12 Words, 1'000 burstcoins
LENGTH = 4
#
####### define words list
words_list = ["like", "just", "love", "know", "never", "want", "time", "out", "there", "make", "look", "eye", "down", "only", "think", "heart", "back", "then", "into", "about", "more", "away", "still", "them", "take", "thing", "even", "through", "long", "always", "world", "too", "friend", "tell", "try", "hand", "thought", "over", "here", "other", "need", "smile", "again", "much", "cry", "been", "night", "ever", "little", "said", "end", "some", "those", "around", "mind", "people", "girl", "leave", "dream", "left", "turn", "myself", "give", "nothing", "really", "off", "before", "something", "find", "walk", "wish", "good", "once", "place", "ask", "stop", "keep", "watch", "seem", "everything", "wait", "got", "yet", "made", "remember", "start", "alone", "run", "hope", "maybe", "believe", "body", "hate", "after", "close", "talk", "stand", "own", "each", "hurt", "help", "home", "god", "soul", "new", "many", "two", "inside", "should", "true", "first", "fear", "mean", "better", "play", "another", "gone", "change", "use", "wonder", "someone", "hair", "cold", "open", "best", "any", "behind", "happen", "water", "dark", "laugh", "stay", "forever", "name", "work", "show", "sky", "break", "came", "deep", "door", "put", "black", "together", "upon", "happy", "such", "great", "white", "matter", "fill", "past", "please", "burn", "cause", "enough", "touch", "moment", "soon", "voice", "scream", "anything", "stare", "sound", "red", "everyone", "hide", "kiss", "truth", "death", "beautiful", "mine", "blood", "broken", "very", "pass", "next", "forget", "tree", "wrong", "air", "mother", "understand", "lip", "hit", "wall", "memory", "sleep", "free", "high", "realize", "school", "might", "skin", "sweet", "perfect", "blue", "kill", "breath", "dance", "against", "fly", "between", "grow", "strong", "under", "listen", "bring", "sometimes", "speak", "pull", "person", "become", "family", "begin", "ground", "real", "small", "father", "sure", "feet", "rest", "young", "finally", "land", "across", "today", "different", "guy", "line", "fire", "reason", "reach", "second", "slowly", "write", "eat", "smell", "mouth", "step", "learn", "three", "floor", "promise", "breathe", "darkness", "push", "earth", "guess", "save", "song", "above", "along", "both", "color", "house", "almost", "sorry", "anymore", "brother", "okay", "dear", "game", "fade", "already", "apart", "warm", "beauty", "heard", "notice", "question", "shine", "began", "piece", "whole", "shadow", "secret", "street", "within", "finger", "point", "morning", "whisper", "child", "moon", "green", "story", "glass", "kid", "silence", "since", "soft", "yourself", "empty", "shall", "angel", "answer", "baby", "bright", "dad", "path", "worry", "hour", "drop", "follow", "power", "war", "half", "flow", "heaven", "act", "chance", "fact", "least", "tired", "children", "near", "quite", "afraid", "rise", "sea", "taste", "window", "cover", "nice", "trust", "lot", "sad", "cool", "force", "peace", "return", "blind", "easy", "ready", "roll", "rose", "drive", "held", "music", "beneath", "hang", "mom", "paint", "emotion", "quiet", "clear", "cloud", "few", "pretty", "bird", "outside", "paper", "picture", "front", "rock", "simple", "anyone", "meant", "reality", "road", "sense", "waste", "bit", "leaf", "thank", "happiness", "meet", "men", "smoke", "truly", "decide", "self", "age", "book", "form", "alive", "carry", "escape", "damn", "instead", "able", "ice", "minute", "throw", "catch", "leg", "ring", "course", "goodbye", "lead", "poem", "sick", "corner", "desire", "known", "problem", "remind", "shoulder", "suppose", "toward", "wave", "drink", "jump", "woman", "pretend", "sister", "week", "human", "joy", "crack", "grey", "pray", "surprise", "dry", "knee", "less", "search", "bleed", "caught", "clean", "embrace", "future", "king", "son", "sorrow", "chest", "hug", "remain", "sat", "worth", "blow", "daddy", "final", "parent", "tight", "also", "create", "lonely", "safe", "cross", "dress", "evil", "silent", "bone", "fate", "perhaps", "anger", "class", "scar", "snow", "tiny", "tonight", "continue", "control", "dog", "edge", "mirror", "month", "suddenly", "comfort", "given", "loud", "quickly", "gaze", "plan", "rush", "stone", "town", "battle", "ignore", "spirit", "stood", "stupid", "yours", "brown", "build", "dust", "hey", "kept", "pay", "phone", "twist", "although", "ball", "beyond", "hidden", "nose", "taken", "fail", "float", "pure", "somehow", "wash", "wrap", "angry", "cheek", "creature", "forgotten", "heat", "rip", "single", "space", "special", "weak", "whatever", "yell", "anyway", "blame", "job", "choose", "country", "curse", "drift", "echo", "figure", "grew", "laughter", "neck", "suffer", "worse", "yeah", "disappear", "foot", "forward", "knife", "mess", "somewhere", "stomach", "storm", "beg", "idea", "lift", "offer", "breeze", "field", "five", "often", "simply", "stuck", "win", "allow", "confuse", "enjoy", "except", "flower", "seek", "strength", "calm", "grin", "gun", "heavy", "hill", "large", "ocean", "shoe", "sigh", "straight", "summer", "tongue", "accept", "crazy", "everyday", "exist", "grass", "mistake", "sent", "shut", "surround", "table", "ache", "brain", "destroy", "heal", "nature", "shout", "sign", "stain", "choice", "doubt", "glance", "glow", "mountain", "queen", "stranger", "throat", "tomorrow", "city", "either", "fish", "flame", "rather", "shape", "spin", "spread", "ash", "distance", "finish", "image", "imagine", "important", "nobody", "shatter", "warmth", "became", "feed", "flesh", "funny", "lust", "shirt", "trouble", "yellow", "attention", "bare", "bite", "money", "protect", "amaze", "appear", "born", "choke", "completely", "daughter", "fresh", "friendship", "gentle", "probably", "six", "deserve", "expect", "grab", "middle", "nightmare", "river", "thousand", "weight", "worst", "wound", "barely", "bottle", "cream", "regret", "relationship", "stick", "test", "crush", "endless", "fault", "itself", "rule", "spill", "art", "circle", "join", "kick", "mask", "master", "passion", "quick", "raise", "smooth", "unless", "wander", "actually", "broke", "chair", "deal", "favorite", "gift", "note", "number", "sweat", "box", "chill", "clothes", "lady", "mark", "park", "poor", "sadness", "tie", "animal", "belong", "brush", "consume", "dawn", "forest", "innocent", "pen", "pride", "stream", "thick", "clay", "complete", "count", "draw", "faith", "press", "silver", "struggle", "surface", "taught", "teach", "wet", "bless", "chase", "climb", "enter", "letter", "melt", "metal", "movie", "stretch", "swing", "vision", "wife", "beside", "crash", "forgot", "guide", "haunt", "joke", "knock", "plant", "pour", "prove", "reveal", "steal", "stuff", "trip", "wood", "wrist", "bother", "bottom", "crawl", "crowd", "fix", "forgive", "frown", "grace", "loose", "lucky", "party", "release", "surely", "survive", "teacher", "gently", "grip", "speed", "suicide", "travel", "treat", "vein", "written", "cage", "chain", "conversation", "date", "enemy", "however", "interest", "million", "page", "pink", "proud", "sway", "themselves", "winter", "church", "cruel", "cup", "demon", "experience", "freedom", "pair", "pop", "purpose", "respect", "shoot", "softly", "state", "strange", "bar", "birth", "curl", "dirt", "excuse", "lord", "lovely", "monster", "order", "pack", "pants", "pool", "scene", "seven", "shame", "slide", "ugly", "among", "blade", "blonde", "closet", "creek", "deny", "drug", "eternity", "gain", "grade", "handle", "key", "linger", "pale", "prepare", "swallow", "swim", "tremble", "wheel", "won", "cast", "cigarette", "claim", "college", "direction", "dirty", "gather", "ghost", "hundred", "loss", "lung", "orange", "present", "swear", "swirl", "twice", "wild", "bitter", "blanket", "doctor", "everywhere", "flash", "grown", "knowledge", "numb", "pressure", "radio", "repeat", "ruin", "spend", "unknown", "buy", "clock", "devil", "early", "false", "fantasy", "pound", "precious", "refuse", "sheet", "teeth", "welcome", "add", "ahead", "block", "bury", "caress", "content", "depth", "despite", "distant", "marry", "purple", "threw", "whenever", "bomb", "dull", "easily", "grasp", "hospital", "innocence", "normal", "receive", "reply", "rhyme", "shade", "someday", "sword", "toe", "visit", "asleep", "bought", "center", "consider", "flat", "hero", "history", "ink", "insane", "muscle", "mystery", "pocket", "reflection", "shove", "silently", "smart", "soldier", "spot", "stress", "train", "type", "view", "whether", "bus", "energy", "explain", "holy", "hunger", "inch", "magic", "mix", "noise", "nowhere", "prayer", "presence", "shock", "snap", "spider", "study", "thunder", "trail", "admit", "agree", "bag", "bang", "bound", "butterfly", "cute", "exactly", "explode", "familiar", "fold", "further", "pierce", "reflect", "scent", "selfish", "sharp", "sink", "spring", "stumble", "universe", "weep", "women", "wonderful", "action", "ancient", "attempt", "avoid", "birthday", "branch", "chocolate", "core", "depress", "drunk", "especially", "focus", "fruit", "honest", "match", "palm", "perfectly", "pillow", "pity", "poison", "roar", "shift", "slightly", "thump", "truck", "tune", "twenty", "unable", "wipe", "wrote", "coat", "constant", "dinner", "drove", "egg", "eternal", "flight", "flood", "frame", "freak", "gasp", "glad", "hollow", "motion", "peer", "plastic", "root", "screen", "season", "sting", "strike", "team", "unlike", "victim", "volume", "warn", "weird", "attack", "await", "awake", "built", "charm", "crave", "despair", "fought", "grant", "grief", "horse", "limit", "message", "ripple", "sanity", "scatter", "serve", "split", "string", "trick", "annoy", "blur", "boat", "brave", "clearly", "cling", "connect", "fist", "forth", "imagination", "iron", "jock", "judge", "lesson", "milk", "misery", "nail", "naked", "ourselves", "poet", "possible", "princess", "sail", "size", "snake", "society", "stroke", "torture", "toss", "trace", "wise", "bloom", "bullet", "cell", "check", "cost", "darling", "during", "footstep", "fragile", "hallway", "hardly", "horizon", "invisible", "journey", "midnight", "mud", "nod", "pause", "relax", "shiver", "sudden", "value", "youth", "abuse", "admire", "blink", "breast", "bruise", "constantly", "couple", "creep", "curve", "difference", "dumb", "emptiness", "gotta", "honor", "plain", "planet", "recall", "rub", "ship", "slam", "soar", "somebody", "tightly", "weather", "adore", "approach", "bond", "bread", "burst", "candle", "coffee", "cousin", "crime", "desert", "flutter", "frozen", "grand", "heel", "hello", "language", "level", "movement", "pleasure", "powerful", "random", "rhythm", "settle", "silly", "slap", "sort", "spoken", "steel", "threaten", "tumble", "upset", "aside", "awkward", "bee", "blank", "board", "button", "card", "carefully", "complain", "crap", "deeply", "discover", "drag", "dread", "effort", "entire", "fairy", "giant", "gotten", "greet", "illusion", "jeans", "leap", "liquid", "march", "mend", "nervous", "nine", "replace", "rope", "spine", "stole", "terror", "accident", "apple", "balance", "boom", "childhood", "collect", "demand", "depression", "eventually", "faint", "glare", "goal", "group", "honey", "kitchen", "laid", "limb", "machine", "mere", "mold", "murder", "nerve", "painful", "poetry", "prince", "rabbit", "shelter", "shore", "shower", "soothe", "stair", "steady", "sunlight", "tangle", "tease", "treasure", "uncle", "begun", "bliss", "canvas", "cheer", "claw", "clutch", "commit", "crimson", "crystal", "delight", "doll", "existence", "express", "fog", "football", "gay", "goose", "guard", "hatred", "illuminate", "mass", "math", "mourn", "rich", "rough", "skip", "stir", "student", "style", "support", "thorn", "tough", "yard", "yearn", "yesterday", "advice", "appreciate", "autumn", "bank", "beam", "bowl", "capture", "carve", "collapse", "confusion", "creation", "dove", "feather", "girlfriend", "glory", "government", "harsh", "hop", "inner", "loser", "moonlight", "neighbor", "neither", "peach", "pig", "praise", "screw", "shield", "shimmer", "sneak", "stab", "subject", "throughout", "thrown", "tower", "twirl", "wow", "army", "arrive", "bathroom", "bump", "cease", "cookie", "couch", "courage", "dim", "guilt", "howl", "hum", "husband", "insult", "led", "lunch", "mock", "mostly", "natural", "nearly", "needle", "nerd", "peaceful", "perfection", "pile", "price", "remove", "roam", "sanctuary", "serious", "shiny", "shook", "sob", "stolen", "tap", "vain", "void", "warrior", "wrinkle", "affection", "apologize", "blossom", "bounce", "bridge", "cheap", "crumble", "decision", "descend", "desperately", "dig", "dot", "flip", "frighten", "heartbeat", "huge", "lazy", "lick", "odd", "opinion", "process", "puzzle", "quietly", "retreat", "score", "sentence", "separate", "situation", "skill", "soak", "square", "stray", "taint", "task", "tide", "underneath", "veil", "whistle", "anywhere", "bedroom", "bid", "bloody", "burden", "careful", "compare", "concern", "curtain", "decay", "defeat", "describe", "double", "dreamer", "driver", "dwell", "evening", "flare", "flicker", "grandma", "guitar", "harm", "horrible", "hungry", "indeed", "lace", "melody", "monkey", "nation", "object", "obviously", "rainbow", "salt", "scratch", "shown", "shy", "stage", "stun", "third", "tickle", "useless", "weakness", "worship", "worthless", "afternoon", "beard", "boyfriend", "bubble", "busy", "certain", "chin", "concrete", "desk", "diamond", "doom", "drawn", "due", "felicity", "freeze", "frost", "garden", "glide", "harmony", "hopefully", "hunt", "jealous", "lightning", "mama", "mercy", "peel", "physical", "position", "pulse", "punch", "quit", "rant", "respond", "salty", "sane", "satisfy", "savior", "sheep", "slept", "social", "sport", "tuck", "utter", "valley", "wolf", "aim", "alas", "alter", "arrow", "awaken", "beaten", "belief", "brand", "ceiling", "cheese", "clue", "confidence", "connection", "daily", "disguise", "eager", "erase", "essence", "everytime", "expression", "fan", "flag", "flirt", "foul", "fur", "giggle", "glorious", "ignorance", "law", "lifeless", "measure", "mighty", "muse", "north", "opposite", "paradise", "patience", "patient", "pencil", "petal", "plate", "ponder", "possibly", "practice", "slice", "spell", "stock", "strife", "strip", "suffocate", "suit", "tender", "tool", "trade", "velvet", "verse", "waist", "witch", "aunt", "bench", "bold", "cap", "certainly", "click", "companion", "creator", "dart", "delicate", "determine", "dish", "dragon", "drama", "drum", "dude", "everybody", "feast", "forehead", "former", "fright", "fully", "gas", "hook", "hurl", "invite", "juice", "manage", "moral", "possess", "raw", "rebel", "royal", "scale", "scary", "several", "slight", "stubborn", "swell", "talent", "tea", "terrible", "thread", "torment", "trickle", "usually", "vast", "violence", "weave", "acid", "agony", "ashamed", "awe", "belly", "blend", "blush", "character", "cheat", "common", "company", "coward", "creak", "danger", "deadly", "defense", "define", "depend", "desperate", "destination", "dew", "duck", "dusty", "embarrass", "engine", "example", "explore", "foe", "freely", "frustrate", "generation", "glove", "guilty", "health", "hurry", "idiot", "impossible", "inhale", "jaw", "kingdom", "mention", "mist", "moan", "mumble", "mutter", "observe", "ode", "pathetic", "pattern", "pie", "prefer", "puff", "rape", "rare", "revenge", "rude", "scrape", "spiral", "squeeze", "strain", "sunset", "suspend", "sympathy", "thigh", "throne", "total", "unseen", "weapon", "weary"]
#
# some informations
print("Words list lenght = %s words"%len(words_list)); # 1626
print("Passphrase possible combinations for %s words = %s combinations"%(LENGTH, len(words_list)**LENGTH);
print("With ~800 passphrase/sec, worst case to find THE passphrase = %s sec ; %s min ; %s hr ; %s days ; %s years"%(len(words_list)**4/800,
                                                                                                                    len(words_list)**4/800/60,
                                                                                                                    len(words_list)**4/800/60/60,
                                                                                                                    len(words_list)**4/800/60/60/24,
                                                                                                                    len(words_list)**4/800/60/60/24/364));
print("Target account = \"%s\""%TARGET)
#
####### generate random words lists
# very very low probability, low calcul power, lottery concept, just luck, so, random lists!
random_words_list1 = []
random_words_list2 = []
random_words_list3 = []
random_words_list4 = []
for word in words_list:
    random_words_list1.append(word)
    random_words_list2.append(word)
    random_words_list3.append(word)
    random_words_list4.append(word)
random.shuffle(random_words_list1)
random.shuffle(random_words_list2)
random.shuffle(random_words_list3)
random.shuffle(random_words_list4)
#
##### testing loop
#
#### passphrase generation
for word1 in random_words_list1:
    for word2 in random_words_list2:
        for word3 in random_words_list3:
            for word4 in random_words_list4:
                passphrase = (word1 + " " + word2 + " " + word3 + " " + word4)
                #
                ### get numeric account number for account
                #
                ## private key from passphrase
                private_key = sha256(passphrase.encode('utf-8')).digest();
                #
                ## public key from curve25519
                curve = Private(secret=private_key);
                public_key = curve.get_public();
                #
                ## hash public key
                hash_public_key = sha256(public_key.serialize()).digest();
                #
                ## create numeric account number from hash
                numeric_account_number = long(0);
                i = 7;
                while i>=0:
                    placevalue = pow(256,i);
                    bytesum = ord(hash_public_key[i]) * placevalue;
                    numeric_account_number += bytesum;
                    i-=1;
                if numeric_account_number == TARGET:
                     sys.exit("Passphrase for target %s is: \"%s\" !"%(TARGET, passphrase));


